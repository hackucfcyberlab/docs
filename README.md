# DRAFT

# Hack@UCF Infrastructure Cluster Documentation

This repo holds any and all forms of documentation pertaining to the currrent Hack@UCF server infrastructure. This includes example configurations, host details, incident reports, tasks to complete, as well as diagrams and invoices.


# Steps to be taken thus far:
* ~~Setup Gitlab Group~~
* ~~Setup gitlab runner~~
* Pipe line files


# Problems that need solving
* User management
* Images


# What we talked about on 8/9/2019
* Terraform pipelines for vm creation
* Provisioning User Access.
* discussing feasibility of certain aspects (Ongoing)

# Primary Technologies used:
* Ansible: For provisioning and configuration
* Terraform: Spinning up vm infrastructure using vmware esxi provider
* Slack: Dealing with user/vm creation (As needed)
* VMware esxi for all the vm provisioning
* AWS s3 bucket for storing terraform state

# What to think about

* Creating VMware templates with packer.
